# Personal website

This is the code for my personal website, hosted at [mitchn.io](https://mitchn.io).

## Stack

- [Hugo](https://gohugo.io/) for static site generation.
- [hugo-coder](https://github.com/luizdepra/hugo-coder) for the Hugo theme.
- [Render](https://render.com) for the static site hosting and SSL.
